import express, {Application, Request, Response} from 'express';
import bodyParser from "body-parser";
import dotenv from 'dotenv';

class Server {

    private app: Application = express();

    constructor() {
        dotenv.config();
        this.app.use(bodyParser.urlencoded({ extended: false}))
        this.app.use(bodyParser.json());
        this.route();
        this.init();
    }

    public route() {
        this.app.use( (req: Request, res: Response) => {
            const USERNAME: string = process.env.USERNAME ?? "Reuno92";
            const PASSWORD: string = process.env.PASSWORD ?? "123456";
            const HASHED = Buffer.from(`${USERNAME}:${PASSWORD}`).toString('base64');
            console.log("TEST", req.headers.authorization?.split('Basic ').join(''), HASHED, req.headers.authorization?.split('Basic ').join('') === HASHED)
            try {
                if (req.headers.authorization?.split('Basic ').join('') === HASHED) {
                    res.setHeader('Content-Type', 'application/json');
                    res.status(200)
                        .send('<img src="https://media.tenor.com/cNmcz6QF2sYAAAAC/some-of-my-best-work-saturday-night-live.gif" alt="Auth ok" /><h1>Yeah! you did it, man.</h1>');
                } else {
                    throw new Error('Error Basic Authentication is wrong');
                }

            } catch(e: Error | unknown) {
                res.status(403)
                    .send('<img src="https://media.tenor.com/mvlys2bMFsgAAAAC/boo-saturday-night-live.gif" alt="Auth wrong" /><h1>It\'s shame!</h1>');
            }
        })
    }

    public init() {
        this.app.listen(4000, () => console.log(`Server works on http://localhost:4000`));
    }
}

new Server();
