# Super tripper Technical Test

## Installation
```bash
yarn
```
or
```bash
npm i
```

> Don't forget to create an environment variable file with `USERNAME` and `PASSWORD` key in a file named `.env` at project root.

## Dependencies
|        Name | Description                          | Link                                             |
|------------:|:-------------------------------------|:-------------------------------------------------|
|         GTS | Google Typescript Style              | [website](https://github.com/google/gts)         |
| Ts-node-dev | Hot Reloader for Node and Typescript | [npm](https://www.npmjs.com/package/ts-node-dev) |
| body-parser | Node.js body parsing middleware.     | [npm](https://www.npmjs.com/package/body-parser) |
|     express | NodeJS Framework                     | [website](https://expressjs.com/)                |

## Project Structure
|          Name | Description                            | Path           |
|--------------:|:---------------------------------------|:---------------|
| Source Folder | Work folder with only Typescript files | ./src          |
|  Build Folder | Transpiled files is located here.      | ./build        |
|  Dependencies | Folder where placed all dependencies   | ./node_modules |

## How to start

### for development
```bash
yarn dev
```
or 
```bash
npm run dev
```

### for production

> You must use docker for create image and a container

For create your image
```bash
docker build . -t supertripper:0.0.1
```

For build a container follow this command
```bash
docker run -d -p 8000:4000 --env-file '.env' --name technicalTestRenaudRacinet supertripper:0.0.1
```

### Support

username is `Reuno92` by default and password is `123456` by default

#### Try with CURL command for:
fail
```bash
curl GET http://localhost:8000
```

success
```bash
curl GET http://localhost:8000 -u Reuno92:123456
```

#### Try with your favorite browser
fail
```bash
http://localhost:8000
```

~~success~~ didn't work :'(
```bash
http://Reuno92:123456@localhost:8000
```

