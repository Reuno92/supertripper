FROM node:18-alpine as build

WORKDIR /usr/

COPY . .

RUN npm i

FROM node:18-alpine as production

WORKDIR /usr/

COPY --from=build ./usr/build .
COPY --from=build ./usr/node_modules ./node_modules/
COPY --from=build ./usr/package*.json .

RUN npm install --global pm2

EXPOSE 4000

CMD ["pm2-runtime", "start", "src/index.js"]
